//
//  ViewController.swift
//  PersistantTimer
//
//  Created by ARZon 27/04/2018.
//  Copyright © 2018 Aleem. All rights reserved.
//

import UIKit

let TIMER_TOTAL_SECONDS = 15

class ViewController: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var timerLable: UILabel!
    
    //MARK: Vars
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var timer = Timer();
    var timerCounter: Int = 0;
    
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialization()
        self.startTimer()
        
    }

    //MARK: Custom Helper Methods
    // initialize background task
    func  initialization()  {
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
        })
    }
    
    //MARK: Timer Methods
    @objc func secondPassed() {
        if timerCounter ==  TIMER_TOTAL_SECONDS {
            self.stopTimer()
        } else {
            timerCounter = timerCounter+1
            timerLable.text =  String(timerCounter)
        }
    }
    
    //trigger Timer
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.secondPassed), userInfo: nil, repeats: true)
    }
    
    // Stop/Invalidate Timer
    func stopTimer() {
        timer.invalidate()
        timerLable.text = "Finished! :)"
    }

}

