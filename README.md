# PersistantTimer
1 - Memory Management
In past we have RC(Reference Count) and this was developer responsibility to keep track of RC and it should
be equal to zero when we're done with some object, but in new approach we've ARC(Automatic Refenece count), and
this reduces a lot of effort of our, ARC keeps track of strong references to instances of classes and increas or decreas
their reference count accordingly. But issue is strong reference cycle like two objects are no longer required, 
but each reference one another. Since each has a non-zero reference count, deallocation, of both objects, can never occur.
To break strong reference cycles, I use weak keyword, weak references, by contrast, don’t increase the strong reference.


2 - Use Autolayout for build iPhone and iPad Apps
Autolayout helps us to build scalable UI that can be used on many devices of different shape and size. I adopt a specific
size view lets say iPhone 6 and adding views with constraint according to desired position, If I want to do this
programmatically I acheive using NSLayoutConstraint. 


3 - Can iOS App works in Background, if so how is it done?
A big YES, If user pressed home button, and launched another app your app goes into background state. Apple provided 
few services that an app can still in in background state, like hiking app to continue to fetch location or download 
some data app or music playing like soundcloud still run when app is in background or lock phone state. 
Apps moving to the background are expected to put themselves into a quiescent state as quickly as possible 
so that they can be suspended by the system. If your app is in the middle of a task and needs a little extra 
time to complete that task, it can call the beginBackgroundTaskWithName:expirationHandler: 
or beginBackgroundTaskWithExpirationHandler: method of the UIApplication object to request some additional execution
time. Calling either of these methods delays the suspension of your app temporarily, giving it a little extra time
to finish its work. Upon completion of that work, your app must call the endBackgroundTask: method to let the system 
know that it is finished and can be suspended. You can find code example in Persistant Timer App.


4 - Image Optimization The simplest way image optimize is to set the frame of UIImageView and set the contentMode 
to one of the resizing options, like I wrote following code. 
+ (UIImage )imageWithImage:(UIImage )image scaledToSize:(CGSize)newSize { 
	UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0); 
	[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext(); 
	UIGraphicsEndImageContext(); 
	return newImage; }